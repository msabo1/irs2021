import numpy as np

def R(theta):
    return np.array([
        [np.cos(theta), np.sin(theta), 0],
        [-np.sin(theta), np.cos(theta), 0],
        [0, 0, 1]
    ], dtype=float)


def rad2deg(theta):
    return theta * 360 / 2 / np.pi