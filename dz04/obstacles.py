import numpy as np
from matplotlib.patches import RegularPolygon

from shared import R

o1_obstacle1_points = np.array([
    [-0.25, -0.25, 0],
    [0.25, -0.25, 0],
    [0.25, 0.25, 0],
    [-0.25, 0.25, 0]
], dtype=float)

i_R_o1 = R(np.pi / 2.2).transpose()
i_t_o1 = np.array([0.75, 0.5, 0], dtype=float)


o2_obstacle2_points = np.array([
    [-0.25, -0.25, 0],
    [0.25, -0.25, 0],
    [0.25, 0.25, 0],
    [-0.25, 0.25, 0]
], dtype=float)

i_R_o2 = R(np.pi / 4).transpose()
i_t_o2 = np.array([-1, 1, 0], dtype=float)

o3_obstacle3_points = np.array([
    [-0.15, -0.6, 0],
    [0.0, -0.6, 0],
    [0.15, 0.6, 0],
    [-0.0, 0.6, 0]
], dtype=float)

i_R_o3 = R(-np.pi / 4).transpose()
i_t_o3 = np.array([-0.6, 0.7, 0], dtype=float)

o4_obstacle4_points = np.append(RegularPolygon((0, 0), 5).get_path().vertices/3, np.zeros((6, 1), dtype=float), axis=1)
i_R_o4 = R(-np.pi / 4).transpose()
i_t_o4 = np.array([1, 0, 0], dtype=float)

o5_obstacle5_points = np.append(RegularPolygon((0, 0), 7).get_path().vertices/3, np.zeros((8, 1), dtype=float), axis=1)
i_R_o5 = R(-np.pi / 4).transpose()
i_t_o5 = np.array([-0.2, 1, 0], dtype=float)

o6_obstacle6_points = np.append(RegularPolygon((0, 0), 5).get_path().vertices/3, np.zeros((6, 1), dtype=float), axis=1)
i_R_o6 = R(-np.pi / 6).transpose()
i_t_o6 = np.array([-1, -0.3, 0], dtype=float)

o7_obstacle7_points = np.append(RegularPolygon((0, 0), 7).get_path().vertices/3, np.zeros((8, 1), dtype=float), axis=1)
i_R_o7 = R(-np.pi / 4).transpose()
i_t_o7 = np.array([-0.5, 0.5, 0], dtype=float)

o8_obstacle8_points = np.append(RegularPolygon((0, 0), 8).get_path().vertices/3, np.zeros((9, 1), dtype=float), axis=1)
i_R_o8 = R(-np.pi / 4).transpose()
i_t_o8 = np.array([-0.5, -1, 0], dtype=float)

o9_obstacle9_points = np.append(RegularPolygon((0, 0), 6).get_path().vertices/3, np.zeros((7, 1), dtype=float), axis=1)
i_R_o9 = R(-np.pi / 4).transpose()
i_t_o9 = np.array([0.3, -0.8, 0], dtype=float)

from polygon import Polygon
from quadtree import QuadTree


class Obstacle:
    def __init__(self, points, R, t):
        self.points = points
        self.R = R
        self.t = t

        self.polygon = Polygon(self[:, :2])

    def __getitem__(self, item):
        return (np.dot(self.points, self.R.transpose()) + self.t)[item]

    def get_bounding_rectangle(self):
        x_min = np.min(self[:, 0])
        x_max = np.max(self[:, 0])
        y_min = np.min(self[:, 1])
        y_max = np.max(self[:, 1])
        return x_min, y_min, x_max - x_min, y_max - y_min


if __name__ == "__main__":
    o = Obstacle(o1_obstacle1_points, i_R_o1, i_t_o1)
    q = QuadTree([o])

