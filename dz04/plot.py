import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle, Circle, Polygon
import numpy as np


class Plot:

    def __init__(self, robot, goal, obstacles, r_from=-2.0, r_to=2.0):
        plt.ion()
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.ax.set_xlim(r_from, r_to)
        self.ax.set_ylim(r_from, r_to)
        self.ax.set_aspect("equal")
        self.ax.set_xticks(np.arange(r_from, r_to, 0.5))
        self.ax.set_yticks(np.arange(r_from, r_to, 0.5))
        plt.grid()
        self.ax.set_xlabel('X')
        self.ax.set_ylabel('Y')

        self.robot = robot
        self.ax.add_patch(Circle(goal.i_g[:2], 0.1, facecolor="#00ff00"))
        self.chassis_patch = self.ax.add_patch(Polygon(self.robot.chassis[:, :2], closed=True, facecolor="#009688"))
        self.wheel_l_patch = self.ax.add_patch(Polygon(self.robot.wheel_l[:, :2], closed=True, facecolor="#212121"))
        self.wheel_r_patch = self.ax.add_patch(Polygon(self.robot.wheel_r[:, :2], closed=True, facecolor="#212121"))

        self.sensors_patches = []
        for sensor in self.robot.sensors:
            self.sensors_patches.append(
                self.ax.add_patch(Polygon(sensor[:, :2], closed=True, facecolor="#00ACC155"))
            )

        for obstacle in obstacles:
            self.ax.add_patch(
                Polygon(obstacle[:, :2], closed=True, facecolor="#ad145788")
            )

    def update(self):
        self.chassis_patch.xy = self.robot.chassis[:, :2]
        self.wheel_l_patch.xy = self.robot.wheel_l[:, :2]
        self.wheel_r_patch.xy = self.robot.wheel_r[:, :2]

        for s_i, sensor_patch in enumerate(self.sensors_patches):
            self.sensors_patches[s_i].xy = self.robot.sensors[s_i][:, :2]

        self.fig.canvas.draw_idle()

