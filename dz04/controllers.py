import numpy as np

from pid import PID
from robot import phi_max, r, s_dist_max
from shared import R


class GoToGoalController:
    def __init__(self, robot, goal):
        self.goal = goal
        self.robot = robot
        self.pid = PID()

    def __call__(self, dt):
        e = np.arctan2(self.goal.i_g[1] - self.robot.i_xi[1], self.goal.i_g[0] - self.robot.i_xi[0]) - self.robot.i_xi[2]
        e = np.arctan2(np.sin(e), np.cos(e))

        r_omega = self.pid(e, dt)
        r_v_dot = 100

        return self.robot.feas(r_v_dot, r_omega)

class StopController:
    def __call__(self, dt):
        return 0, 0

class AvoidObstacleController:
    def __init__(self, robot):
        self.robot = robot
        self.pid = PID()

    def __call__(self, dt):
        i_xi = np.array([0, 0, 0], dtype=float)
        for sensor in self.robot.sensors_array:
            s_m_inv = np.array([-(s_dist_max - sensor.d), 0, 0], dtype=float)
            R_m_inv = R(sensor.r_s[2]).T @ s_m_inv + np.array([sensor.r_s[0], sensor.r_s[1], 0], dtype=float)
            I_m_inv = R(self.robot.i_xi[2]).T @ R_m_inv + np.array([self.robot.i_xi[0], self.robot.i_xi[1], 0], dtype=float)
            I_s = R(self.robot.i_xi[2]).T @ sensor.r_s + np.array([self.robot.i_xi[0], self.robot.i_xi[1], 0], dtype=float)
            i_xi += I_m_inv - I_s
        e = np.arctan2(i_xi[1], i_xi[0]) - self.robot.i_xi[2]
        e = np.arctan2(np.sin(e), np.cos(e))

        r_omega = self.pid(e, dt)
        return self.robot.feas(100, r_omega)
