from obstacles import (i_R_o4, i_R_o5, i_R_o6, i_R_o7, i_R_o8, i_R_o9, i_t_o4,
                       i_t_o5, i_t_o6, i_t_o7, i_t_o8, i_t_o9,
                       o4_obstacle4_points, o5_obstacle5_points,
                       o6_obstacle6_points, o7_obstacle7_points,
                       o8_obstacle8_points, o9_obstacle9_points)
from plot import Plot, plt
from robot import Robot, l, np


class Goal:
    def __init__(self, i_g=np.zeros(3, dtype=float)):
        self.i_g = i_g

from controllers import (AvoidObstacleController, GoToGoalController,
                         StopController)
from obstacles import *
from quadtree import QuadTree

if __name__ == "__main__":
    robot = Robot()
    goal = Goal(np.array([-1, 1, 0], dtype=float))

    obstacles = [
        Obstacle(o4_obstacle4_points, i_R_o4, i_t_o4),
        Obstacle(o5_obstacle5_points, i_R_o5, i_t_o5),
    ]

    plot = Plot(robot, goal, obstacles)
    dt = 0.010
    plt.draw()

    quad_tree = QuadTree(obstacles)

    gtgc = GoToGoalController(robot, goal)
    aoc = AvoidObstacleController(robot)
    sc = StopController()

    ac = gtgc

    t = 0
    while True:
        t += dt
        r_v_dot, r_omega = ac(dt)
        phi_dot_l, phi_dot_r = robot.r_inverse_kinematics(r_v_dot, r_omega)
        i_xi_dot = robot.forward_kinematics(phi_dot_l, phi_dot_r)
        robot.update_state(i_xi_dot, dt)
        robot.update_sensors(quad_tree)
        if robot.avoiding:
            ac = aoc
        elif np.sqrt((robot.i_xi[0] - goal.i_g[0]) ** 2 + (robot.i_xi[1] - goal.i_g[1]) ** 2) < l:
            ac = sc
            break
        else:
            ac = gtgc

        collision_detected = robot.detect_collision(quad_tree)
        if collision_detected:
            ac = sc
            print("Collision")
            break
        plot.update()
        plt.pause(dt)

    robot = Robot([1,1,0])
    goal = Goal(np.array([-1.5, -1, 0], dtype=float))

    obstacles = [
        Obstacle(o6_obstacle6_points, i_R_o6, i_t_o6),
        Obstacle(o7_obstacle7_points, i_R_o7, i_t_o7),
        Obstacle(o8_obstacle8_points, i_R_o8, i_t_o8),
        Obstacle(o9_obstacle9_points, i_R_o9, i_t_o9),
    ]

    plot = Plot(robot, goal, obstacles)
    dt = 0.010
    plt.draw()

    quad_tree = QuadTree(obstacles)

    gtgc = GoToGoalController(robot, goal)
    aoc = AvoidObstacleController(robot)
    sc = StopController()

    ac = gtgc

    t = 0
    while True:
        t += dt
        r_v_dot, r_omega = ac(dt)
        phi_dot_l, phi_dot_r = robot.r_inverse_kinematics(r_v_dot, r_omega)
        i_xi_dot = robot.forward_kinematics(phi_dot_l, phi_dot_r)
        robot.update_state(i_xi_dot, dt)
        robot.update_sensors(quad_tree)
        if robot.avoiding:
            ac = aoc
        elif np.sqrt((robot.i_xi[0] - goal.i_g[0]) ** 2 + (robot.i_xi[1] - goal.i_g[1]) ** 2) < l:
            ac = sc
        else:
            ac = gtgc

        collision_detected = robot.detect_collision(quad_tree)
        if collision_detected:
            ac = sc
            print("Collision")
            break
        plot.update()
        plt.pause(dt)

    plt.waitforbuttonpress()
