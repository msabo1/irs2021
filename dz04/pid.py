class PID:
    def __init__(self, k_p=2.0, k_i=0.5, k_d=0.5):
        self.k_p = k_p
        self.k_i = k_i
        self.k_d = k_d

        self.e_acc = 0
        self.e_prev = 0

    def __call__(self, e, dt):
        e_diff = (e - self.e_prev) / dt
        self.e_prev = e
        self.e_acc += e * dt
        return self.k_p * e + self.k_i * self.e_acc + self.k_d * e_diff