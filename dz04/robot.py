import numpy as np

from shared import R

# wheel base / 2
l = 0.1
# wheel radius
r = 0.05
# robot length
d = 0.25
# wheel width
wheel_w = 0.03
# wheel L
alpha_l = np.pi / 2
beta_l = 0
# wheel R
alpha_r = -np.pi / 2
beta_r = np.pi

# npr. maksimalna kutna brzina kotaca je 200 okr/min
phi_max = 200 * 2 * np.pi / 60

wheel_points = [
    np.array([-wheel_w / 2, -r, 0]),
    np.array([0, -r - 0.01, 0]),
    np.array([wheel_w / 2, -r, 0]),
    np.array([wheel_w / 2, r, 0]),
    np.array([-wheel_w / 2, r, 0])
]

chassis_point = [
    np.array([-0.07, -l, 0]),
    np.array([d, -l, 0]),
    np.array([d, l, 0]),
    np.array([-0.07, l, 0]),
]

wheel_points_m = np.array(wheel_points)
chassis_point_m = np.array(chassis_point)

s_dist_max = 0.5
s_dist_min = 0.05
s_alpha = np.deg2rad(20)

start_avoid_dist = 0.66*s_dist_max
stop_avoid_dist = 0.99*s_dist_max

class Sensor:
    def __init__(self, r_s=np.array([0, 0, 0], dtype=float)):
        self.r_s = r_s
        self.d = s_dist_max

    @property
    def s_cone(self):
        return np.array([
            [s_dist_min * np.cos(s_alpha / 2), s_dist_min * np.sin(s_alpha / 2), 0],
            [self.d * np.cos(s_alpha / 2), self.d * np.sin(s_alpha / 2), 0],
            [self.d, 0, 0],
            [self.d * np.cos(s_alpha / 2), -self.d * np.sin(s_alpha / 2), 0],
            [s_dist_min * np.cos(s_alpha / 2), -s_dist_min * np.sin(s_alpha / 2), 0],
        ])

    @property
    def r_cone(self):
        return np.dot(self.s_cone, R(self.r_s[2])) + np.array([self.r_s[0], self.r_s[1], 0], dtype=float)

    @property
    def s_cone_full(self):
        return np.array([
            [0, 0, 0],
            [s_dist_max * np.cos(s_alpha / 2), s_dist_max * np.sin(s_alpha / 2), 0],
            [s_dist_max, 0, 0],
            [s_dist_max * np.cos(s_alpha / 2), -s_dist_max * np.sin(s_alpha / 2), 0],
        ])

    @property
    def r_cone_full(self):
        return np.dot(self.s_cone_full, R(self.r_s[2])) + np.array([self.r_s[0], self.r_s[1], 0], dtype=float)


sensors_points = np.array([
    [d, l, np.pi / 2],
    [d, l * 2 / 3, np.pi / 2 * 2 / 3],
    [d, l * 1 / 3, np.pi / 2 * 1 / 3],
    [d, 0, 0],
    [d, -l * 1 / 3, -np.pi / 2 * 1 / 3],
    [d, -l * 2 / 3, -np.pi / 2 * 2 / 3],
    [d, -l, -np.pi / 2],
], dtype=float)

from polygon import Polygon
from rectangle import Rectangle


class Robot:
    def __init__(self, i_xi=np.array([0, 0, 0], dtype=float)):
        self.i_xi = i_xi

        self.sensors_array = []
        for sensor_points in sensors_points:
            self.sensors_array.append(Sensor(sensor_points))

        self.avoiding = False


    @property
    def sensors(self):
        return np.dot(
            np.array([sensor.r_cone for sensor in self.sensors_array]),
            R(self.i_xi[2])
        ) + np.array([self.i_xi[0], self.i_xi[1], 0], dtype=float)

    @property
    def wheel_l(self):
        # prvo zarotirati točku kotača u njegovom kordinatnom sustavu za beta
        # onda translatirati za l, i onda zarotirati za alpha u kordinatnom sustavu robota
        # onda zarotirati za njegovu rotaciju (i_xi[2], odn theta) i onda translatirati s obzirom na globalni kordinatni
        return np.dot(np.dot(np.dot(wheel_points_m, R(beta_l)) + np.array([l, 0, 0]), R(alpha_l)),
                         R(self.i_xi[2])) + np.array([self.i_xi[0], self.i_xi[1], 0])

    @property
    def wheel_r(self):
        return np.dot(np.dot(np.dot(wheel_points_m, R(beta_r)) + np.array([l, 0, 0]), R(alpha_r)), R(self.i_xi[2])) + np.array([self.i_xi[0], self.i_xi[1], 0])

    @property
    def chassis(self):
        # šasija je malo lakša jer je u koordinatnom sustavu robota, dakle w.r.t. na koordinatni sustav robota, ne radimo transformaciju
        return np.dot(chassis_point_m, R(self.i_xi[2])) + np.array([self.i_xi[0], self.i_xi[1], 0])

    def r_inverse_kinematics(self, r_v_dot, r_omega):
        phi_dots = 1 / r * np.array([
            [1, 0, -l],
            [1, 0, l],
            [0, 1, 0]
        ], dtype=float) @ np.array([
            r_v_dot, 0, r_omega
        ], dtype=float)

        phi_dot_l = phi_dots[0]
        phi_dot_r = phi_dots[1]
        return phi_dot_l, phi_dot_r

    def inverse_kinematics(self, v_dot, omega):
        # pretvoriti transl. brzinu v i kutnu brzinu omega
        # u x_dot, y_dot i omega, i onda to pretvoriti preko
        # invezne kinematike u phi_dots
        phi_dots = 1 / r * np.array([
            [1, 0, -l],
            [1, 0, l],
            [0, 1, 0]
        ], dtype=float) @ R(self.i_xi[2]) @ np.array([
            v_dot * np.cos(self.i_xi[2]),
            v_dot * np.sin(self.i_xi[2]),
            omega
        ], dtype=float)

        phi_dot_l = phi_dots[0]
        phi_dot_r = phi_dots[1]
        return phi_dot_l, phi_dot_r
    
    def feas(self, r_v_dot, r_omega):
        phi_dot_l, phi_dot_r = self.r_inverse_kinematics(r_v_dot, r_omega)

        phi_dot_ld = phi_dot_l
        phi_dot_rd = phi_dot_r

        phi_dot_lr_max = max(phi_dot_r, phi_dot_l)
        phi_dot_lr_min = min(phi_dot_r, phi_dot_l)
        
        if phi_dot_lr_max > phi_max:
            phi_dot_ld = phi_dot_l - (phi_dot_lr_max - phi_max)
            phi_dot_rd = phi_dot_r - (phi_dot_lr_max - phi_max)
        if phi_dot_lr_min < 0:
            phi_dot_ld = phi_dot_l - phi_dot_lr_min
            phi_dot_rd = phi_dot_r - phi_dot_lr_min

        phi_dot_ld = max(0, min(phi_max, phi_dot_ld))
        phi_dot_rd = max(0, min(phi_max, phi_dot_rd))

        feas = np.array([
            [0.5, 0.5, 0],
            [0, 0, 1],
            [-1/(2*l), 1/(2*l), 0]
        ], dtype=float) @ np.array([r * phi_dot_ld, r * phi_dot_rd, 0], dtype=float)
        r_v_dot_feas = feas[0]
        r_omega_feas = feas[2]
        return r_v_dot_feas, r_omega_feas

    def forward_kinematics(self, phi_dot_l, phi_dot_r):
        return R(self.i_xi[2]).transpose() @ np.array(
            [[1 / 2, 1 / 2, 0], [0, 0, 1], [-1 / 2 / l, 1 / 2 / l, 0]]) @ np.array([r * phi_dot_l, r * phi_dot_r, 0])

    def update_state(self, i_xi_dot, dt):
        self.i_xi += i_xi_dot * dt

    def update_sensors(self, quad_tree):
        global_min = s_dist_max
        for sensor in self.sensors_array:
            i_cone_full = np.dot(
                sensor.r_cone_full,
                R(self.i_xi[2])
            ) + np.array([self.i_xi[0], self.i_xi[1], 0], dtype=float)

            x_min = np.min(i_cone_full[:, 0])
            x_max = np.max(i_cone_full[:, 0])
            y_min = np.min(i_cone_full[:, 1])
            y_max = np.max(i_cone_full[:, 1])

            rectangle = Rectangle((x_min, y_min, x_max - x_min, y_max - y_min))

            polygon = Polygon(i_cone_full[:, :2])

            i_sensor_x = i_cone_full[0, 0]
            i_sensor_y = i_cone_full[0, 1]

            min_dist = s_dist_max
            for obstacle in quad_tree.find_items(rectangle):
                contact_points = polygon.intersection_points(obstacle.polygon)
                for i_obstacle_x, i_obstacle_y in contact_points:
                    dist = np.sqrt((i_sensor_x - i_obstacle_x) ** 2 + (i_sensor_y - i_obstacle_y) ** 2)
                    if dist < min_dist:
                        min_dist = dist

            sensor.d = min_dist
            if sensor.d < global_min:
                global_min = sensor.d
        if global_min < 0.66*s_dist_max:
            self.avoiding = True
        if global_min > 0.99*s_dist_max:
            self.avoiding = False


    def detect_collision(self, quad_tree):
        all_points = np.concatenate([
            self.wheel_l,
            self.wheel_r,
            self.chassis
        ])

        x_min = np.min(all_points[:, 0])
        x_max = np.max(all_points[:, 0])
        y_min = np.min(all_points[:, 1])
        y_max = np.max(all_points[:, 1])

        rectangle = Rectangle((x_min, y_min, x_max - x_min, y_max - y_min))

        polygon = Polygon(all_points[:, :2])

        for obstacle in quad_tree.find_items(rectangle):
            collision = polygon.collidepoly(obstacle.polygon)
            if isinstance(collision, bool):
                if not collision:
                    continue
            return True
        return False
