from plot import Plot, plt
from robot import Robot, l, np


class Goal:
    def __init__(self, i_g=np.zeros(3, dtype=float)):
        self.i_g = i_g

from controllers import GoToGoalController, StopController
from obstacles import *
from quadtree import QuadTree

if __name__ == "__main__":
    robot = Robot()
    goal = Goal(np.array([-1, 1, 0], dtype=float))

    obstacles = [
        Obstacle(o1_obstacle1_points, i_R_o1, i_t_o1),
        Obstacle(o2_obstacle2_points, i_R_o2, i_t_o2)
    ]

    plot = Plot(robot, goal, obstacles)
    dt = 0.010
    plt.draw()

    quad_tree = QuadTree(obstacles)

    gtgc = GoToGoalController(robot, goal)
    sc = StopController()

    ac = gtgc

    t = 0
    while True:
        t += dt

        if np.sqrt((robot.i_xi[0] - goal.i_g[0]) ** 2 + (robot.i_xi[1] - goal.i_g[1]) ** 2) < l:
            ac = sc
        else:
            ac = gtgc

        r_v_dot, r_omega = ac(dt)
        phi_dot_l, phi_dot_r = robot.r_inverse_kinematics(r_v_dot, r_omega)
        i_xi_dot = robot.forward_kinematics(phi_dot_l, phi_dot_r)
        robot.update_state(i_xi_dot, dt)
        robot.update_sensors(quad_tree)

        collision_detected = robot.detect_collision(quad_tree)
        if collision_detected:
            ac = sc
            print("Collision")
            break

        plot.update()
        plt.pause(dt)

    plt.waitforbuttonpress()
