import matplotlib.pyplot as plt
import numpy as np

plt.ion()
fig = plt.figure(figsize=(8, 8))

ax = fig.add_subplot(111, projection="3d")
ax.set_xlim(-1, 1)
ax.set_ylim(-1, 1)
ax.set_zlim(-1, 1)
ax.set_xlabel('X')
ax.set_ylabel('Y')
ax.set_zlabel('Z')


def DH(params):
  a = params[0]
  alpha = params[1]
  d = params[2]
  theta = params[3]
  return np.array([
    [np.cos(theta), -np.sin(theta) * np.cos(alpha), np.sin(theta) * np.sin(alpha), a * np.cos(theta)],
    [np.sin(theta), np.cos(theta) * np.cos(alpha), -np.cos(theta) * np.sin(alpha), a * np.sin(theta)],
    [0, np.sin(alpha), np.cos(alpha), d],
    [0, 0, 0, 1]
  ], dtype=float)


def update_plot():
  global axes_list
  global lines_list

  frame_last = None
  for i in range(manipulator_params.shape[0]):
    T_i = np.eye(4, dtype=float)
    for j in range(i + 1):
      T_i = T_i @ DH(manipulator_params[j])
    frame_i = frame.dot(T_i.transpose())

    if 0 < i < manipulator_params.shape[0] - 1:
      axes_list[i]._offsets3d = [frame_i[0, 0]], [frame_i[0, 1]], [frame_i[0, 2]]
    else:
      axes_list[i]._offsets3d = frame_i[:, 0], frame_i[:, 1], frame_i[:, 2]

    if i > 0:
      lines_list[i-1].set_data_3d([frame_last[0, 0], frame_i[0, 0]], [frame_last[0, 1], frame_i[0, 1]], [frame_last[0, 2], frame_i[0, 2]])
    frame_last = frame_i
  fig.canvas.draw_idle()
  plt.pause(dt)



if __name__ == "__main__":
  #okvir
  frame = np.array([
    [0, 0, 0, 1],
    [1, 0, 0, 1],
    [0, 1, 0, 1],
    [0, 0, 1, 1]
  ], dtype=float)
  # scale-at ću osi na 20 cm
  # da možemo pratiti što se događa
  frame[:, :3] = frame[:, :3] * 0.2

  # Load params
  manipulator_params = np.load("./stanford_params.npy")
  joint_infos = np.load("./stanford_joints.npy")
  colors = np.load("./stanford_colors.npy")
  joint_labels = np.load("./stanford_labels.npy")
  T_g = np.load("./stanford_goal.npy")

  dt = 0.01


  axes_list = []
  lines_list = []

  frame_last = None

  # 1. OVO SU FRAMEOVI SVAKE POVEZNICE
  for i in range(manipulator_params.shape[0]):
    T_i = np.eye(4, dtype=float)
    for j in range(i + 1):
      T_i = T_i @ DH(manipulator_params[j])
    frame_i = frame.dot(T_i.transpose())
    if i == manipulator_params.shape[0] - 1:
      T_e = T_i

    if 0 < i < manipulator_params.shape[0] - 1:
      axes_list.append(ax.scatter(frame_i[0, 0], frame_i[0, 1], frame_i[0, 2], color=colors[i], marker="."))
    else:
      axes_list.append(ax.scatter(frame_i[:, 0], frame_i[:, 1], frame_i[:, 2], color=colors[i], marker="."))

    # 2. LINIJE KOJE POVEZUJU (i - 1). i i. poveznicu
    if i > 0:
      line = ax.plot([frame_last[0, 0], frame_i[0, 0]], [frame_last[0, 1], frame_i[0, 1]], [frame_last[0, 2], frame_i[0, 2]], color=colors[i], linewidth=4, solid_capstyle="round")[0]
      lines_list.append(line)
    frame_last = frame_i



  T_e = np.eye(4, dtype=float)
  for params in manipulator_params:
    T_e = T_e @ DH(params)


  frame_g = frame.dot(T_g.transpose())
  ax.scatter(frame_g[:, 0], frame_g[:, 1], frame_g[:, 2], color="#ff1744", marker="*")

  p = T_g[0:3, 3] - manipulator_params[6,2] * T_g[0:3,2]

  theta1 = 2 * np.arctan2(-p[0] + (p[0]**2 + p[1]**2 - manipulator_params[2, 2]**2)**0.5, manipulator_params[2, 2] + p[1])
  c_1 = np.cos(theta1)
  s_1 = np.sin(theta1)
  
  d_3 = ((p[0] * c_1 + p[1] * s_1)**2 + p[2]**2)**0.5
  theta2 = np.arctan2(p[0] * c_1 + p[1] * s_1, p[2])

  c_2 = np.cos(theta2)
  s_2 = np.sin(theta2)
      
  d_2 = manipulator_params[2, 2]
  
  T_3 = np.array([
    [c_1 * c_2, -s_1, c_1 * s_2, c_1 * s_2 * d_3 - s_1 * d_2],
    [s_1 * c_2, c_1, s_1 * s_2, s_1 * s_2 * d_3 + c_1 * d_2],
    [-s_2, 0, c_2, c_2 * d_3],
    [0, 0, 0, 0]
  ], dtype = float)

  three_R_6 = T_3.T @ T_g 


  theta4 = np.arctan2(three_R_6[1, 2], three_R_6[0, 2])
  theta5 = np.arctan2((three_R_6[0, 2]**2 + three_R_6[1, 2]**2)**0.5, three_R_6[2, 2])
  theta6 = np.arctan2(three_R_6[2, 1], -three_R_6[2, 0])

  q_g = np.array([theta1, theta2, d_3, theta4, theta5, theta6], dtype=float)

  p_gain = 0.8

  while True: 
    q_t = np.array([manipulator_params[1, 3], manipulator_params[2, 3],
                  manipulator_params[3, 2], manipulator_params[4, 3],
                  manipulator_params[5, 3], manipulator_params[6, 3]], dtype=float)
    q_dot = ((q_g - q_t) / dt) * p_gain

    for k, j_info in enumerate(joint_infos):
      i = int(j_info[2])
      j = int(j_info[3])
      q_dot[k] = np.sign(q_dot[k]) * min(j_info[4], abs(q_dot[k]))
      new_value = manipulator_params[i, j] + q_dot[k] * dt
      if j_info[0] <= new_value <= j_info[1]:
          manipulator_params[i, j] = new_value
  
    update_plot()

  
