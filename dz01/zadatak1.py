import matplotlib.pyplot as plt
import numpy as np
import scipy.linalg as linalg

from skew import skew


def animate(initialPoints, omega, scatter, dt, time):
  RDot = linalg.expm(skew(omega*dt))
  R = np.eye(3)

  points = np.zeros(initialPoints.shape, dtype=float)
  while True:
    time -= dt
    R = RDot @ R
    for j in range(initialPoints.shape[0]):
      points[j] = R @ initialPoints[j]
    scatter._offsets3d = points[:, 0], points[:, 1], points[:, 2]
    fig.canvas.draw_idle()
    if time <= 0:
      break
    plt.pause(dt)
  return points

plt.ion()
fig = plt.figure()
ax = fig.add_subplot(111, projection="3d")

ax.set_xlim(-5, 5)
ax.set_ylim(-5, 5)
ax.set_zlim(-5, 5)
ax.set_xlabel("x")
ax.set_ylabel("x")
ax.set_zlabel("x")

points_initial = np.array([
  [1, 0, 0],
  [0, 1, 0],
  [0, 0, 1],
  [0, 0, 0]
], dtype=float)

points = points_initial

sc = ax.scatter(points_initial[:, 0], points_initial[:, 1], points_initial[:, 2])

dt = 0.01

omega = np.array([0, 0.25*np.pi, 0])
newInitialPoints = animate(points_initial, omega, sc, dt, 1)


omega = np.array([np.pi, 0, 0])
animate(newInitialPoints, omega, sc, dt, 1)
