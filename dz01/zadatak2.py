import matplotlib.pyplot as plt
import numpy as np
import scipy.linalg as linalg

from skew import skew


def animate(initialPoints1, initialPoints2, omega1, omega2, pDot1, pDot2, scatter1, scatter2, dt, time):
  R1Dot = linalg.expm(skew(omega1*dt))
  R2Dot = linalg.expm(skew(omega2*dt))

  R1 = np.eye(3)
  R2 = np.eye(3)
  p1 = np.array([0, 0, 0], dtype=float)
  p2 = np.array([0, 0, 0], dtype=float)

  points1 = np.zeros(initialPoints1.shape, dtype=float)
  points2 = np.zeros(initialPoints2.shape, dtype=float)
  while True:
    time -= dt
    R1 = R1Dot @ R1
    R2 = R2Dot @ R2
    p1 += pDot1*dt
    p2 += pDot2*dt
    for j in range(points_initial1.shape[0]):
      points1[j] = R1 @ initialPoints1[j] + p1
      points2[j] = R1 @ (R2 @ initialPoints2[j] + p2) + p1
    scatter1._offsets3d = points1[:, 0], points1[:, 1], points1[:, 2]
    scatter2._offsets3d = points2[:, 0], points2[:, 1], points2[:, 2]
    fig.canvas.draw_idle()
    if time <= 0:
      break
    plt.pause(dt)

plt.ion()
fig = plt.figure()
ax = fig.add_subplot(111, projection="3d")

ax.set_xlim(-5, 5)
ax.set_ylim(-5, 5)
ax.set_zlim(-5, 5)
ax.set_xlabel("x")
ax.set_ylabel("y")
ax.set_zlabel("z")

points_initial1 = np.array([
  [1, 0, 0],
  [0, 1, 0],
  [0, 0, 1],
  [0, 0, 0]
], dtype=float)

points_initial2 = np.array([
  [1, 0, 0],
  [0, 1, 0],
  [0, 0, 1],
  [0, 0, 0]
], dtype=float)

sc1 = ax.scatter(points_initial1[:, 0], points_initial1[:, 1], points_initial1[:, 2], c='blue')
sc2 = ax.scatter(points_initial2[:, 0], points_initial2[:, 1], points_initial2[:, 2], c='red')

dt = 0.01

omega1 = np.array([0, 0, 0], dtype=float)
pDot1 = np.array([1, 1, 0], dtype=float)
omega2 = np.array([np.pi, -np.pi, 0], dtype=float)
pDot2 = np.array([2, 0 , 2], dtype=float)

animate(points_initial1, points_initial2, omega1, omega2, pDot1, pDot2, sc1, sc2, dt, 1)

omega1 = np.array([2*np.pi, 0, 0], dtype=float)
pDot1 = np.array([1, 1, 0], dtype=float)
omega2 = np.array([0, 8*np.pi, 0], dtype=float)
pDot2 = np.array([-2, 0 , -3], dtype=float)
animate(points_initial1, points_initial2, omega1, omega2, pDot1, pDot2, sc1, sc2, dt, 1)
