import matplotlib.pyplot as plt
import numpy as np
from matplotlib.patches import Polygon


def init_plot(r_from=-1, r_to=2):
  plt.ion()
  fig = plt.figure()
  ax = fig.add_subplot(111)
  ax.set_xlim(r_from, r_to)
  ax.set_ylim(r_from, r_to)
  ax.set_aspect("equal")
  # mreža (ax.set_xticks, ...)
  plt.grid()
  ax.set_xlabel("x")
  ax.set_ylabel("y")
  return fig, ax


def R(theta):
  return np.array([
    [np.cos(theta), np.sin(theta), 0],
    [-np.sin(theta), np.cos(theta), 0],
    [0, 0, 1]
  ], dtype=float)

def generateM(alpha1, alpha2, alpha3, beta1, beta2, beta3, l, d):
   return np.array([
        [np.sin(alpha1+beta1), -np.cos(alpha1+beta1), -l * np.cos(beta1)],
        [np.sin(alpha2+beta2), -np.cos(alpha2+beta2), -l * np.cos(beta2)],
        [np.sin(alpha3+beta3), -np.cos(alpha3+beta3), -l * np.cos(beta3)],
        [np.cos(alpha1+beta1), np.sin(alpha1+beta1), d + l * np.sin(beta1)],
        [np.cos(alpha2+beta2), np.sin(alpha2+beta2), d + l * np.sin(beta2)],
        [np.cos(alpha3+beta3), np.sin(alpha3+beta3), d + l * np.sin(beta3)]
    ], dtype=float)

if __name__ == "__main__":
  fig, ax = init_plot()
  plt.draw()

  # inicijalno stanje robota
  i_xi = np.array([0, 0, 0], dtype=float)
  # stanje robota u gibajućem koordinatnom sustavu uvijek [0, 0, 0]
  l = 0.32
  d = 0.1
  r = 0.05

  # širina kotača
  wheel_w = 0.04

  alpha_1 = np.pi / 5
  alpha_2 = -np.pi / 5
  alpha_3 = np.pi
  beta_1 = -alpha_1
  beta_2 = -alpha_2 + (13/10)*np.pi
  beta_3 = 0.0

  wheel_points = [
    np.array([-wheel_w / 2, -(2*r +d), 0], dtype=float),
    np.array([wheel_w / 2, -(2*r +d), 0], dtype=float),
    np.array([wheel_w / 2, -(2*r +d) + 2*r, 0]),
    np.array([wheel_w / 2 - 0.015, -(2*r +d) + 2*r, 0]),
    np.array([wheel_w / 2 - 0.015, -(2*r +d) + 2*r + d - 0.02, 0]),
    np.array([wheel_w / 2 - 0.005, -(2*r +d) + 2*r + d - 0.02, 0]),
    np.array([wheel_w / 2 - 0.005, -(2*r +d) + 2*r + d, 0]),
    np.array([-wheel_w / 2 + 0.005, -(2*r +d) + 2*r + d, 0]),
    np.array([-wheel_w / 2 + 0.005, -(2*r +d) + 2*r + d - 0.02, 0]),
    np.array([-wheel_w / 2 + 0.015, -(2*r +d) + 2*r + d - 0.02, 0]),
    np.array([-wheel_w / 2 + 0.015, -(2*r +d) + 2*r, 0]),
    np.array([-wheel_w / 2, -(2*r +d) + 2*r, 0], dtype=float)

  ]

  chassis_points = [
    np.array([-l, -l * np.sin(alpha_1), 0]),
    np.array([l * np.cos(alpha_2), -l*np.sin(alpha_1), 0]),
    np.array([l*np.cos(alpha_1), l*np.sin(alpha_1), 0]),
    np.array([-l, l* np.sin(alpha_1), 0]),
  ]


  wheel_1 = np.array([
    R(i_xi[2]).transpose() @ R(alpha_1).transpose() @ (R(beta_1).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
  ])

  wheel_2 = np.array([
    R(i_xi[2]).transpose() @ R(alpha_2).transpose() @ (R(beta_2).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
  ])

  wheel_3 = np.array([
    R(i_xi[2]).transpose() @ R(alpha_3).transpose() @ (R(beta_3).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
  ])

  chassis = np.array([
    R(i_xi[2]).transpose() @ point + np.array([i_xi[0], i_xi[1], 0]) for point in chassis_points
  ])

  wheel_1_patch = ax.add_patch(Polygon(wheel_1[:, :2], facecolor="#000000"))
  wheel_2_patch = ax.add_patch(Polygon(wheel_2[:, :2], facecolor="#000000"))
  wheel_3_patch = ax.add_patch(Polygon(wheel_3[:, :2], facecolor="#000000"))
  chassis_patch = ax.add_patch(Polygon(chassis[:, :2], facecolor="#0000ff88"))


  dt = 0.01
  time = 0.0

  v_dot = 0.5
  omega = np.pi/2

  while True:
    if time >= 1:
      omega = 0
      v_dot = 1
    if time >= 2:
      v_dot = 0.2
      omega = -np.pi
    if time >= 3.25:
      v_dot = 0.8
    if time >= 4:
      omega = np.pi
    if time >= 5.01:
      omega = 0
      v_dot = 1
    if time >= 6.3:
      break

    M = generateM(alpha_1, alpha_2, alpha_3, beta_1, beta_2, beta_3, l, d)
    Minv = np.linalg.pinv(M)
    
    phi_dots = M @ R(i_xi[2]) @ np.array([v_dot * np.cos(i_xi[2]), v_dot * np.sin(i_xi[2]), omega], dtype=float)
    i_xi_dot = R(i_xi[2]).transpose() @ Minv @ phi_dots
    i_xi += i_xi_dot * dt
    
    phi_dots[3:6] *= -1/d
    
    beta_1 += phi_dots[3] * dt
    beta_2 += phi_dots[4] * dt
    beta_3 += phi_dots[5] * dt

    wheel_1 = np.array([
        R(i_xi[2]).transpose() @ R(alpha_1).transpose() @ (R(beta_1).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
    ])

    wheel_2 = np.array([
        R(i_xi[2]).transpose() @ R(alpha_2).transpose() @ (R(beta_2).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
    ])
    
    wheel_3 = np.array([
        R(i_xi[2]).transpose() @ R(alpha_3).transpose() @ (R(beta_3).transpose() @ point + np.array([l, 0, 0])) + np.array([i_xi[0], i_xi[1], 0]) for point in wheel_points
    ])

    chassis = np.array([
        R(i_xi[2]).transpose() @ point + np.array([i_xi[0], i_xi[1], 0]) for point in chassis_points
    ])

    wheel_1_patch.xy = wheel_1[:, :2]
    wheel_2_patch.xy = wheel_2[:, :2]
    wheel_3_patch.xy = wheel_3[:, :2]
    chassis_patch.xy = chassis[:, :2]

    fig.canvas.draw_idle()
    plt.pause(dt)

    time += dt

